import sys
import json
import yaml
import DataStore
import Notes
from tqdm import tqdm

from Notes import Note
from pushbullet import Pushbullet

PB_API_KEY = 'o.IAdjWkBwZZqC2p4TJZkYGF1FIJrP0nXy'
TARGET_DEVICE = 'Galaxy S7'

#-------------------------------------------------------------------------------
def PushNote():
    push = device.push_note('This is a note', 'Chomp!')

#-------------------------------------------------------------------------------
def Clone(obj):
    if isinstance(obj, list):
        obj = [Clone(o) for o in obj]
    elif isinstance(obj, dict):
        obj = {(Clone(k), Clone(v)) for k, v in obj.items()}
    print(type(obj))
    return obj

#-------------------------------------------------------------------------------
def LoadPushesFromServer():
    pb = Pushbullet(PB_API_KEY)
    # device = pb.get_device(TARGET_DEVICE)

    pushes = pb.get_pushes()

    with open('push_cache.json', 'w') as f:
        json.dump(pushes, f, indent=2)

    return pushes

#-------------------------------------------------------------------------------
def LoadPushesFromDisk():
    with open('pushes.json') as f:
        pushes = json.load(f)
    # pushes_data = Notes.Load()
    # pushes = yaml.load(pushes_data)
    return pushes

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    pushes = LoadPushesFromServer()
    # pushes = LoadPushesFromDisk()

    Notes.Load()
    Notes.AddPushes(pushes)
    Notes.Save()
