import win32serviceutil
import win32service
import win32event
import servicemanager
import time
import sys
import os
import traceback
import logging
import shlex
import yaml
from logging.handlers import RotatingFileHandler

# s_RootPath = os.path.abspath(os.path.dirname(sys.argv[0]))
s_RootPath = os.path.abspath(os.path.dirname(__file__))

kServiceClientTimeout = 2.5
kServiceStopTimeout   = 2.5

#-------------------------------------------------------------------------------
class ServiceHost(win32serviceutil.ServiceFramework):
    _svc_name_ = 'ServiceHost'
    _svc_display_name_ = 'ServiceHost'

    def __init__(self, args):
        try:
            win32serviceutil.ServiceFramework.__init__(self, args)
            self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
            self.isAlive = False

            try:
                config_path = os.path.join(s_RootPath, 'config.yaml')
                with open(config_path) as f:
                    self.config = yaml.load(f)

                script_dir = self.config['ScriptDir']
                script_file = os.path.basename(self.config['ScriptFile'])
                script_args = self.config['ScriptArgs']
                log_file = self.config['LogFile']
            except Exception as e:
                print(traceback.format_exc())

            self.scriptPath = os.path.join(script_dir, script_file)
            self.logPath = os.path.join(script_dir, log_file)
            self.scriptArgs = script_args

            self.log = logging.getLogger(f'{ServiceHost._svc_name_}.{script_file}')
            self.log.setLevel(logging.DEBUG)
            self.logHandler = RotatingFileHandler(self.logPath, maxBytes=64 * 1024, backupCount=10)

            formatter = logging.Formatter('[%(asctime)s] (%(name)s) <%(levelname)s> %(message)s')
            self.logHandler.setFormatter(formatter)
            self.log.addHandler(self.logHandler)
            self.log.info('Service initialized.')
        except Exception as e:
            print(traceback.format_exc())

    def SvcStop(self):
        self.isAlive = False
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        self.log.info('Shutting down...')
        self.logHandler.flush()
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        self.isAlive = True
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))
        self.log.info('Starting...')
        self.logHandler.flush()
        try:
            code = self.Run()
            if code:
                self.log.error(f'Finished with code {code}.')
            else:
                self.log.info(f'Finished.')
        except Exception as e:
            print(traceback.format_exc())
            print(e)
            self.log.critical('Finished with errors:')
            self.log.critical(e)
            self.log.critical(traceback.format_exc())

        try:
            self.logHandler.flush()
            self.logHandler.doRollover()
        except Exception as e:
            print(traceback.format_exc())

    def Run(self):
        import subprocess
        from subprocess import Popen, TimeoutExpired, STDOUT
        proc = subprocess.run(['python', self.scriptPath, *shlex.split(self.scriptArgs)], capture_output=True)
        self.log.info(proc.stdout.decode('utf-8'))
        self.log.info(proc.stderr.decode('utf-8'))
        self.logHandler.flush()
        return
        # proc = Popen(['python', self.scriptPath, *shlex.split(self.scriptArgs)])

        while True:
            try:
                retval = proc.wait(kServiceClientTimeout)
                return retval
            except TimeoutExpired as e:
                pass

            retval = win32event.WaitForSingleObject(self.hWaitStop, kServiceStopTimeout * 1000)
            if retval != win32event.WAIT_TIMEOUT:
                self.log.warn('Terminating process...')
                self.logHandler.flush()
                proc.kill()
                return None

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(ServiceHost)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(ServiceHost)
