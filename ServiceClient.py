
import click
import os
import time
from win10toast import ToastNotifier

s_DataPath = 'service_client_data.txt'

try:
   with open(s_DataPath) as f:
      data = f.readline()

   s_InstanceId = int(data) + 1
except:
   s_InstanceId = 1

with open(s_DataPath, 'w') as f:
   f.write(str(s_InstanceId))

#---------------------------------------
@click.command()
@click.option('--duration', '-d', default=4.0, help='Amount of time for which to show the toast')
@click.option('--interval', '-i', default=4.0, help='Interval between toasts')
@click.argument('title')
@click.argument('body')
def Notify(title, body, duration, interval):
   message_count = 0
   while True:
      message_count += 1
      ToastNotifier().show_toast(title % s_InstanceId, body % message_count, duration=duration)
      time.sleep(interval)

def main():
   Notify()

if __name__ == '__main__':
   main()
