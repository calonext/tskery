import dropbox

#-------------------------------------------------------------------------------
ACCESS_TOKEN = 'i-6R7qImmiAAAAAAAABeujDrFG1s_gZNVoZyNw8BppwRHJAtaIgr_6hIk7DVFuJK'
APP_DIR = '/Apps/Tskery'

#-------------------------------------------------------------------------------
def Load(path):
    if not path:
        return None

    dbx = dropbox.Dropbox(ACCESS_TOKEN)
    # files = dbx.files_list_folder('').entries[0]

    if path[0] != '/':
        path = '/' + path

    fmeta, response = dbx.files_download(path)

    try:
        return response.content.decode('utf-8')
    except Exception as e:
        # print('Unable to load "%s" as utf-8.' % path)
        return response.content

#-------------------------------------------------------------------------------
def Save(path, data):
    dbx = dropbox.Dropbox(ACCESS_TOKEN)
    # import code
    # code.interact(local=locals())
    # files = dbx.files_list_folder('').entries[0]

    if path[0] != '/':
        path = '/' + path

    return dbx.files_upload(data.encode('utf-8'), path, mode=dropbox.files.WriteMode.overwrite)
