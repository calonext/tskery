import DataStore
import yaml
from datetime import datetime

PUSH_CACHE_FILENAME = '/push_cache.yaml'
DATA_FILENAME = '/data.yaml'

#-------------------------------------------------------------------------------
class Note:
    def __init__(self):
        self.Title = ''
        self.Detail = ''
        self.CreationTime = None
        self.DueTime = None
        self.Links = None
        self.Tags = []
        self.PushId = None

    @staticmethod
    def Create(title, desc, due_time=None, links=[], tags=[], push_id=None):
        note = Note()
        note.Title = title
        note.Detail = desc
        note.CreationTime = datetime.now()
        note.DueTime = due_time
        note.Links = links[:]
        note.Tags = tags[:]
        note.PushId = push_id
        return note

    @staticmethod
    def EnsureFromPushData(push_data):
        note = FindFromPushData(push_data)
        if note:
            return note

        note = Note.CreateFromPushData(push_data)
        Add(note)
        return note

    @staticmethod
    def CreateFromPushData(push_data):
        push_type = push_data['type']
        links = []
        if push_type == 'link':
            links.append(push_data['url'])
        # elif push_type == 'note':
        #     print('[note] %s' % push['body'].replace('\n', '\n       '))
        # else:
        #     print('[%s] <unknown>' % push_type)
        title = push_data.get('title', '[untitled]')
        body = push_data.get('body', '[empty]')
        push_id = push_data.get('iden', None)
        note = Note.Create(title, body, links=links, push_id=push_id)
        return note

    def __repr__(self):
        return '[%s] %s' % (self.Title, self.Detail)

    def __str__(self):
        return '[%s] %s' % (self.Title, self.Detail)

s_Notes = []

#-------------------------------------------------------------------------------
def Load():
    data = DataStore.Load(DATA_FILENAME)
    global s_Notes
    s_Notes = yaml.load(data)
    if not s_Notes:
        s_Notes = []

#-------------------------------------------------------------------------------
def Save():
    notes_str = yaml.dump(s_Notes)
    DataStore.Save(DATA_FILENAME, notes_str)

#-------------------------------------------------------------------------------
def GetAll():
    return s_Notes[:]

#-------------------------------------------------------------------------------
def Add(note):
    assert(not any(n.PushId == note.PushId for n in s_Notes))
    s_Notes.append(note)

#-------------------------------------------------------------------------------
def AddPushes(pushes):
    total_count = len(pushes)
    pushes = [p for p in pushes if not FindFromPushData(p)]
    print('Skipping %s old pushes...' % (total_count - len(pushes)))
    print('Adding %s new pushes...' % len(pushes))
    for push in pushes:
        Note.EnsureFromPushData(push)

#-------------------------------------------------------------------------------
def Remove(note):
    s_Notes.remove(note)

#-------------------------------------------------------------------------------
def FindFromPushData(push_data):
    if not push_data:
        return None

    if 'iden' not in push_data:
        return None

    global s_Notes
    assert(s_Notes != None)

    iden = push_data['iden']
    for note in s_Notes:
        if note.PushId == iden:
            return note
    return None
