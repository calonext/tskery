from flask import Flask, send_from_directory
from Notes import Note
from pushbullet import Listener
from pushbullet import Pushbullet

import Render
import Notes
import random
import DataStore
import jinja2
import logging

PB_API_KEY = 'o.IAdjWkBwZZqC2p4TJZkYGF1FIJrP0nXy'
pb = Pushbullet(PB_API_KEY)

#-------------------------------------------------------------------------------
app = Flask(__name__, static_url_path='/static')

#-------------------------------------------------------------------------------
g_Count = 0

#-------------------------------------------------------------------------------
def DebugBreak():
    import code
    code.interact(local=locals())

#-------------------------------------------------------------------------------
class Obj:
    def __init__(self, title, link):
        self.Title = title
        self.Link = link

#-------------------------------------------------------------------------------
@app.route('/static/<path:path>')
def SendStatic(path):
    return send_from_directory('static', path)

#-------------------------------------------------------------------------------
@app.route('/')
def Test():
    Admin.PushNote()
    Notes.Load()
    notes = Notes.GetAll()
    rendered = Render.Generate(notes=notes, count=g_Count)
    return rendered, 200

#-------------------------------------------------------------------------------
@app.route('/notes/list/')
def ListNotes():
    Notes.Load()
    return str(Notes.GetAll()), 200


#-------------------------------------------------------------------------------
def HandlePush(data):
    print('<<%s>>' % data)
    global g_Count
    g_Count += 1


import Admin
print('THIS IS A TEST')
print(__name__)

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)

    log = logging.getLogger('pushbullet.Listener')
    print(log)
    print(log_handler)
    log.addHandler(log_handler)

    log.debug('this is a test')
    listener = Listener(pb, on_push=HandlePush)
    print('Starting listener...')
    listener.start()
    print('Listener started!')

    app.run(debug=True)
    listener.close()
