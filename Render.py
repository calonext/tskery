from jinja2 import Environment, PackageLoader, select_autoescape

env = Environment(
    loader=PackageLoader('Server', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


def Generate(**kwarg):
    template = env.get_template('index.html')
    return template.render(**kwarg)
